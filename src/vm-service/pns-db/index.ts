import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";

const config = new pulumi.Config();
const sharedKey: string = config.require("sharedKey");
const pnsDbAmi: string = config.require("pnsdb-ami");
const pnsDbInstanceType: string = config.require("pnsdb-instance-type");
const serviceName = "PNS-DB"

export class PnsDb {
  ec2Instance: aws.ec2.Instance;

  constructor(
    subnetId: pulumi.Output<any>,
    vpcId: pulumi.Output<any>
  ) {
    const sharedCommonKey = new aws.ec2.KeyPair(serviceName, {
      publicKey: sharedKey,
    });

    const secgrp = new aws.ec2.SecurityGroup(serviceName, {
      vpcId: vpcId,
      description: "default-ec2",
      ingress: [
        {
          protocol: "tcp",
          fromPort: 22,
          toPort: 22,
          cidrBlocks: ["0.0.0.0/0"],
        },
      ],
      egress: [
        {
          protocol: "-1",
          fromPort: 0,
          toPort: 0,
          cidrBlocks: ["0.0.0.0/0"],
        }
      ]
    });

    this.ec2Instance = new aws.ec2.Instance(serviceName, {
      tags: {
        Name: "PNS-DB"
      },
      instanceType: pnsDbInstanceType,
      ami: pnsDbAmi,
      subnetId: subnetId,
      keyName: sharedCommonKey.keyName,
      vpcSecurityGroupIds: [secgrp.id],
    });
  }
}
