import { Config, StackReference, Output } from "@pulumi/pulumi";

const config = new Config();
const infrastructure = new StackReference(
  config.require("infrastructure")
);

export const PUBLIC_SUBNET_1 = infrastructure.getOutput("publicSubnet1");
export const WILDCARD_CERTIFICATE_ARN = infrastructure.getOutput("wildCardCertificate");
export const MAIN_VPC = infrastructure.getOutput("vpcId");
export const DOMAIN_NAME = infrastructure.getOutput("domainName");

export const CLUSTER_OIDC_PROVIDER_URL = infrastructure.getOutput(
  "clusterOidcProviderUrl"
);
export const CLUSTER_OIDC_PROVIDER_ARN = infrastructure.getOutput(
  "clusterOidcProviderArn"
);
export const KUBE_CONFIG = infrastructure.getOutput("kubeConfig");
