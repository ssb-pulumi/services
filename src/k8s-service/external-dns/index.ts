import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import {
  CLUSTER_OIDC_PROVIDER_ARN,
  CLUSTER_OIDC_PROVIDER_URL,
} from "../../types";

export class ExternalDns {
  readonly externalDns: k8s.helm.v3.Chart;

  constructor(provider: k8s.Provider, domainName: pulumi.Output<any>) {
    const saName = "external-dns";

    const saAssumeRolePolicy = pulumi
      .all([CLUSTER_OIDC_PROVIDER_ARN, CLUSTER_OIDC_PROVIDER_URL])
      .apply(([arn, url]: [any, any]) =>
        aws.iam.getPolicyDocument({
          statements: [
            {
              actions: ["sts:AssumeRoleWithWebIdentity"],
              conditions: [
                {
                  test: "StringEquals",
                  values: [`system:serviceaccount:default:${saName}`],
                  variable: `${url}:sub`,
                },
                {
                  test: "StringEquals",
                  values: ["sts.amazonaws.com"],
                  variable: `${url}:aud`,
                },
              ],
              effect: "Allow",
              principals: [
                {
                  identifiers: [arn],
                  type: "Federated",
                },
              ],
            },
          ],
        })
      );

    const saRole = new aws.iam.Role("external-dns", {
      assumeRolePolicy: saAssumeRolePolicy.json,
    });

    const sa = new k8s.core.v1.ServiceAccount(
      saName,
      {
        metadata: {
          name: saName,
          annotations: {
            "eks.amazonaws.com/role-arn": saRole.arn,
          },
        },
      },
      { provider: provider }
    );

    const rolePolicyAttachment = new aws.iam.RolePolicyAttachment(
      "external-dns",
      {
        role: saRole,
        policyArn: aws.iam.ManagedPolicy.AmazonRoute53FullAccess,
      }
    );

    const externalDnsClusterRole = new k8s.rbac.v1.ClusterRole(
      saName,
      {
        metadata: {
          labels: {
            "app.kubernetes.io/name": "external-dns",
          },
        },
        rules: [
          {
            apiGroups: [""],
            resources: ["services"],
            verbs: ["get", "list", "watch"],
          },
          {
            apiGroups: [""],
            resources: ["pods"],
            verbs: ["get", "list", "watch"],
          },
          {
            apiGroups: ["extensions"],
            resources: ["ingresses"],
            verbs: ["get", "list", "watch"],
          },
          {
            apiGroups: [""],
            resources: ["nodes"],
            verbs: ["list"],
          },
          {
            apiGroups: [""],
            resources: ["endpoints"],
            verbs: ["get", "list", "watch"],
          },
        ],
      },
      { provider: provider }
    );

    const externalDnsRoleBinding = new k8s.rbac.v1.ClusterRoleBinding(
      saName,
      {
        metadata: {
          namespace: "default",
          labels: {
            "app.kubernetes.io/name": "external-dns",
          },
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: sa.metadata.name,
            namespace: "default",
          },
        ],
        roleRef: {
          kind: "ClusterRole",
          name: externalDnsClusterRole.metadata.name,
          apiGroup: "rbac.authorization.k8s.io",
        },
      },
      { provider: provider }
    );

    this.externalDns = new k8s.helm.v3.Chart(
      saName,
      {
        //repo: "bitnami",
        chart: "external-dns",
        fetchOpts: {
          repo: "https://charts.bitnami.com/bitnami",
        },
        values: {
          provider: "aws",
          sources: ["service", "ingress"],
          policy: "sync",
          aws: {
            zoneType: "public",
          },
          registry: "txt",
          image: {
            tag: "0.7.3-debian-10-r0",
          },
          rbac: {
            create: false,
          },
          serviceAccount: {
            create: false,
            name: sa.metadata.name,
          },
          domainFilters: [domainName],
        },
      },
      { provider: provider }
    );
  }
}
