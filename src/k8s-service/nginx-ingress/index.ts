import * as k8s from "@pulumi/kubernetes";
import { WILDCARD_CERTIFICATE_ARN } from "../../types";

export class NginxIngress {
  readonly nginxIngress: k8s.helm.v3.Chart;

  constructor(provider: k8s.Provider) {
    this.nginxIngress = new k8s.helm.v3.Chart(
      "nginx-ingress",
      {
        chart: "nginx-ingress",
        fetchOpts: {
          repo: "https://charts.helm.sh/stable",
        },
        values: {
          controller: {
            image: {
              tag: "v0.34.1",
            },
            kind: "Deployment",
            replicaCount: 2,
            stats: {
              enabled: true,
            },
            metrics: {
              enabled: true,
            },
            updateStrategy: {
              type: "RollingUpdate",
            },
            publishService: {
              enabled: true,
            },
            service: {
              type: "LoadBalancer",
              annotations: {
                "service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout": 300,
                "service.beta.kubernetes.io/aws-load-balancer-ssl-cert": WILDCARD_CERTIFICATE_ARN
              },
              targetPorts: {
                http: "http",
                https: "http"
              }
            },
            config: {
              "proxy-buffer-size": "16k",
            },
          },
        },
      },
      { provider: provider }
    );
  }
}
