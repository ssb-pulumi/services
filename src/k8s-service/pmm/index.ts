import * as k8s from "@pulumi/kubernetes";
import { DOMAIN_NAME } from "../../types";

export class Pmm {
  readonly pmm: k8s.helm.v3.Chart;

  constructor(provider: k8s.Provider) {
    this.pmm = new k8s.helm.v3.Chart(
      "pmm",
      {
        chart: "pmm-server",
        fetchOpts: { repo: "https://percona-charts.storage.googleapis.com" },
        values: {
          platform: "kubernetes",
          service: {
            type: "LoadBalancer",
          },
          imageTag: "2.13.0",
          credentials: {
            password: "password"
          }
        },
      },
      { provider: provider }
    );

    const pmmIngress = DOMAIN_NAME.apply(dn => {
      new k8s.networking.v1beta1.Ingress(
        "pmm",
        {
          metadata: {
            namespace: "default",
            labels: {
              app: "pmm"
            },
            annotations: {
              "kubernetes.io/ingress.class": "nginx"
            }
          },
          spec: {
            rules: [
              {
                host: `pmm.${dn}`,
                http: {
                  paths: [
                    {
                      path: "/",
                      backend: {
                        serviceName: "pmm-service",
                        servicePort:  "http"
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        { provider: provider }
      );
    }); 
  }
}
