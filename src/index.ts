import * as k8s from "@pulumi/kubernetes";
import { PnsDb } from "./vm-service/pns-db";
import { Pmm } from "./k8s-service/pmm";
import { NginxIngress } from "./k8s-service/nginx-ingress";
import { ExternalDns } from "./k8s-service/external-dns";
import { Config, StackReference, Output } from "@pulumi/pulumi";
import { MAIN_VPC, PUBLIC_SUBNET_1, DOMAIN_NAME, KUBE_CONFIG } from "./types";

const provider = new k8s.Provider("k8s", { kubeconfig: KUBE_CONFIG });

// Kubernetes App services
const pmm = new Pmm(provider);
const nginxIngress = new NginxIngress(provider);
const externalDns = new ExternalDns(provider, DOMAIN_NAME);

// Apps running on EC2
const pnsDb = new PnsDb(PUBLIC_SUBNET_1, MAIN_VPC);

export const pmmUrl = pmm.pmm.urn;
export const pnsDbServerIP = pnsDb.ec2Instance.publicIp;
export const pnsDbServerPrivateIP = pnsDb.ec2Instance.privateIp;
